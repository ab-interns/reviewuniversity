//
//  shoeReviewsTableViewController.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 2/14/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

import UIKit

class showReviewsTableViewController: UITableViewController {
    
    var itemsUser : [Users] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func addReviewTapped(_ sender: Any) {
        showAlertReview ()
    }
    
    func showAlertReview () {
        let alert = UIAlertController(title: "Teacher", message: "in your University", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "save", style: .default)
        { action in
            let textField = alert.textFields![0]
            let nameUsers = Users(nameUsers: textField.text!)
            
            self.itemsUser.append(nameUsers!)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField {
            textNameUniversity in textNameUniversity.placeholder = "Enter Review Teacher"
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)

    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsUser.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as UITableViewCell
        let nameUsers = itemsUser[indexPath.row]
        cell.textLabel?.text = nameUsers.nameUsers
        cell.detailTextLabel?.text = "ชื่อผู้รีวิว"
        return cell
        
    }
   
}
