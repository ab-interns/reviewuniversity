//
//  SecondTableViewController.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 1/26/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

import Foundation
import UIKit

class showTeacherTableViewController: UITableViewController {
    
    var itemsTeacher : [Teacher] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - Action Teacher
    
    @IBAction func addButtonTapped(_ sender: Any) {
        showAlertTeacher()
    }
    func showAlertTeacher(){
        let alert = UIAlertController(title: "Teacher", message: "in your University", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "save", style: .default)
        { action in
            let textField = alert.textFields![0]
            let nameTeachers = Teacher(name: textField.text!)
            
            self.itemsTeacher.append(nameTeachers)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField {
            textNameUniversity in textNameUniversity.placeholder = "Enter Teacher in your University"
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)

    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsTeacher.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SecondCell", for: indexPath) as UITableViewCell
        let nameTeachers = itemsTeacher[indexPath.row]
        cell.textLabel?.text = nameTeachers.name
        return cell
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let target = segue.destination
        let selectedRowIndex = tableView.indexPathForSelectedRow // เมื่อผู้ใช้กดเลือก row
        let cell = tableView.cellForRow(at: selectedRowIndex!)
        target.title = cell?.textLabel?.text
        
    }
}
