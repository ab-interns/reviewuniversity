//
//  ReviewUserController.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 2/15/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

class ReviewUserController {
    static let shared = ReviewUserController()
    
    var reviews: [Review] = []
    private init() {
        // Private initialization to ensure just one instance is created.
    }
    
    func addUniversity(userreviews: Review) {
        reviews.append(userreviews)
    }
    
    func removeAll() {
        reviews.removeAll()
    }
}
