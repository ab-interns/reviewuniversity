//
//  user.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 2/15/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

class Users {
    //var id: String!
    var nameUsers: String!
    init?(nameUsers: String) {
        
        // name must not be empty
        guard !nameUsers.isEmpty else {
            return nil
        }
        
        self.nameUsers = nameUsers
    }
}
