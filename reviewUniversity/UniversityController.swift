//
//  UniversityController.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 1/28/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

class UniversityController {
    static let shared = UniversityController()
    
    var universitys: [University] = []
    private init() {
        // Private initialization to ensure just one instance is created.
    }
    
    func addUniversity(university: University) {
        universitys.append(university)
    
    }
    
    func removeAll() {
        universitys.removeAll()
    }
}
