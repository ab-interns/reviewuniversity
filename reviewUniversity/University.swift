//
//  University.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 1/28/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

class University {
    //var id: String!
    var name: String!
    var teachers: [Teacher] = []
    init(name: String) {
        self.name = name
    }
}
