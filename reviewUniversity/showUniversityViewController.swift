//
//   เรียก cell     } FirstTable.swift
//  reviewUniversity
//
//  Created by Supatchareporn arworn on 1/26/2560 BE.
//  Copyright © 2560 Supatchareporn arworn. All rights reserved.
//

import UIKit

class showUniversityViewController: UITableViewController {

    
    var items = UniversityController.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Table view data source

//    func showInsertNewDataPopup() {
//        print("insert data")
////        let name = "MIT"
////        let university = University(name: name)
////        UniversityController.shared.addUniversity(university: university)
//        
//        tableView.reloadData()
//    }

    // MARK: - Action 
    
    @IBAction func addButtonTapped(_ sender: AnyObject) {
        // Show Popup for insert new data        
//        showInsertNewDataPopup()
        showAlertUniversity()
        
    }
    
    func showAlertUniversity() {
        let alert = UIAlertController(title: "university", message: "Add your University", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "save", style: .default)
        { action in
            let textField = alert.textFields![0]
            let nameUniversity = University(name: textField.text!)
            
            self.items.addUniversity(university: nameUniversity)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField {
            textNameUniversity in textNameUniversity.placeholder = "Enter your University"
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UITableView
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return nameUniversity.count
        return UniversityController.shared.universitys.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let university = UniversityController.shared.universitys[indexPath.row]
        cell.textLabel?.text = university.name
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let target = segue.destination
        let selectedRowIndex = tableView.indexPathForSelectedRow // เมื่อผู้ใช้กดเลือก row
        let cell = tableView.cellForRow(at: selectedRowIndex!)
        target.title = cell?.textLabel?.text
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
}
